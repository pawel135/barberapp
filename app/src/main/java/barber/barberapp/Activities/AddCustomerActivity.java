package barber.barberapp.Activities;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import barber.barberapp.R;
import barber.barberapp.Model.Customer;

import java.util.Arrays;


public class AddCustomerActivity extends Sidebar {
    private TextView resultTxt;
    private EditText nameField;
    private EditText surnameField;
    private EditText emailField;
    private EditText phoneField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_add_customer, null, false);
        drawer.addView(contentView, 0);

        resultTxt = (TextView) findViewById(R.id.resultTextView);
        nameField = (EditText) findViewById(R.id.customerNameField);
        surnameField = (EditText) findViewById( R.id.customerSurnameField);
        emailField = (EditText) findViewById( R.id.customerEmailField );
        phoneField = (EditText) findViewById(R.id.customerNumberField );

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Pola zostały wyczyszczone", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                nameField.setText(""); surnameField.setText(""); emailField.setText(""); phoneField.setText("");

            }
        });
    }


    public void addCustomerToList(View view) {
        String[] data = {
                nameField.getText().toString().trim(),
                surnameField.getText().toString().trim(),
                emailField.getText().toString().trim(),
                phoneField.getText().toString().trim()
        };

        Customer client = new Customer( data );
        if( client.add( client, this.getApplicationContext() ) )
            resultTxt.setText("Dodano klienta");
        else
            resultTxt.setText("Błąd podczas dodawania klienta");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                            // Navigate up to the closest parent
                            .startActivities();
                } else {
                    // This activity is part of this app's task, so simply
                    // navigate up to the logical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);


    }
}