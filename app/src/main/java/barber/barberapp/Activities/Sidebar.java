package barber.barberapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import barber.barberapp.Activities.CustomerList.CustomerListActivity;
import barber.barberapp.Activities.VisitList.VisitListActivity;
import barber.barberapp.Model.Auth;
import barber.barberapp.R;
import barber.barberapp.SignIn;

public class Sidebar extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sidebar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0); // header representing Upper side of sidebar (application name + user name)
        TextView text = (TextView) headerLayout.findViewById(R.id.textView);

        text.setText(SignIn.getUsernameStr());

        if (!Auth.userIsAdmin(this.getApplicationContext())) {
            navigationView.getMenu().getItem(0).setEnabled(false); // 0 means 1. button, 1 means 2. ...
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sidebar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_customers) {
            Intent cl = new Intent(getBaseContext(), CustomerListActivity.class);
            startActivity(cl);
            finish();
        } else if (id == R.id.nav_visits) {
            Intent vl = new Intent(getBaseContext(), VisitListActivity.class);
            startActivity(vl);
            finish();
        } else if (id == R.id.nav_addCustomer) {
            Intent addCustomerIntent = new Intent(getBaseContext(), AddCustomerActivity.class);
            startActivity(addCustomerIntent);
            finish();
        } else if (id == R.id.nav_addVisit) {
            Intent vl = new Intent(getBaseContext(), AddVisitActivity.class);
            startActivity(vl);
            finish();
        } else if (id == R.id.nav_logout) {
            Auth.logout(this.getApplicationContext());
            Intent signin = new Intent(this, SignIn.class);
            startActivity(signin);
            finish();
        } else if (id == R.id.nav_barber) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
