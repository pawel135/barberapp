package barber.barberapp.Activities.VisitList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import barber.barberapp.Activities.CustomerList.ClientAdapter;
import barber.barberapp.Model.Customer;
import barber.barberapp.Model.Visit;
import barber.barberapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by micha on 10.05.2016.
 */

public class VisitAdapter extends ArrayAdapter {
    private List<Visit> visitList = null;
    private ItemFilter mFilter = new ItemFilter();

    public VisitAdapter(Context context, int resource ) {
        super(context, resource);
        visitList = Visit.getAll(context);
    }

    private static class DataHandler{
        TextView id;
        TextView date;
    }

    @Override
    public int getCount() {
        return this.visitList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.visitList.get(position);
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent ) {
        View row;
        row = convertView;
        DataHandler handler;
        if( convertView == null ) {
            LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.visit_list_row, parent, false);
            handler = new DataHandler();
            handler.id = (TextView)row.findViewById(R.id.id);
            handler.date = (TextView)row.findViewById(R.id.date);
            row.setTag(handler);
        } else {
            handler = (DataHandler)row.getTag();
        }

        Visit visit;
        visit = (Visit)this.getItem(position);
        String id = "ID klienta: " + visit.fieldName[0];
        String date = visit.fieldName[1] + "\ngodz. " + visit.fieldName[2];
        handler.id.setText( id );
        handler.date.setText( date );

        return row;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final List<Visit> list = visitList;
            int count = list.size();
            final List<Visit> nlist = new ArrayList<>(count);
            Visit filterableVisit;

            for (int i = 0; i < count; i++) {
                filterableVisit = list.get(i);
                for (int j = 0; j < filterableVisit.fieldName.length; j++) {
                    if (filterableVisit.fieldName[j].toLowerCase().contains(filterString)) {
                        nlist.add(filterableVisit);
                    }
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            visitList = (List<Visit>) results.values;
            notifyDataSetChanged();
        }
    }
}